#!python3.6.8
# -*- coding: utf-8 -*-
#参考サイトオアンダPythonAPI_V20ライブラリ
# 　リファレンス
# http://developer.oanda.com/rest-live-v20/introduction/
# https://oanda-api-v20.readthedocs.io/en/latest/index.html
# 　使い方
# http://www.algo-fx-blog.com/fx-historical-big-data-api/
# http://www.algo-fx-blog.com/fx-historical-big-data-api/
# https://qiita.com/THERE2/items/f716565c884e7750c6c1

#OandaAPI
from oandapyV20 import API
from oandapyV20.exceptions import V20Error
from oandapyV20.endpoints.pricing import PricingStream
import oandapyV20.endpoints.orders as orders
import oandapyV20.endpoints.instruments as instruments
import json
import configparser
import datetime
import pandas as pd
from dateutil.relativedelta import relativedelta

#Acount指定
config = configparser.ConfigParser()
config.read('./config.txt')     #パス指定する
accountID = config['oanda']['accountID']
access_token = config['oanda']['access_token']
api = API(access_token=access_token, environment="practice")

#通貨情報について
import oandapyV20.endpoints.accounts as accounts
params = { "instruments": "EUR_USD,EUR_JPY,USD_JPY" }
r = accounts.AccountInstruments(accountID=accountID, params=params)
request_data = api.request(r)
print(type(request_data))
print("-----通貨情報-----")
print(json.dumps(request_data, indent=2))

#口座情報の取得
import oandapyV20.endpoints.accounts as accounts
r = accounts.AccountSummary(accountID)
request_data = api.request(r)
print(type(request_data))
print("-----口座情報-----")
print(json.dumps(request_data, indent=2))

#価格データの取得
# S5/S10/S15/S30 : それぞれ5秒足、10秒足、15秒足、30秒足
# M1/M2/M4/M5/M10/M15/M30 : それぞれ1分足、2分足、4分足、5分足、10分足、15分足、30分足
# H1/H2/H3/H4/H6/H12 : それぞれ1時間足、2時間足、3時間足、4時間足、6時間足、8時間足、12時間足
# D : 日足
# W : 週足
# M : 月足
import oandapyV20.endpoints.instruments as instruments
params = {
  "count": 5,
  "granularity": "M5"
}
r = instruments.InstrumentsCandles(instrument="USD_JPY", params=params)
request_data = api.request(r)
print(type(request_data))
print("-----価格データ-----")
print(json.dumps(request_data, indent=2))


#2016年1月から2019年9月までの10分足データを取得
#一ヶ月づつ取得
year_months =[
    [2019, 1], [2019, 2], [2019, 3], [2019, 4], [2019, 5], [2019, 6], [2019, 7], [2019, 8], [2019, 9], [2019, 10], [2019, 11], [2019, 12],
]
#指定した期間のcandleデータを取得する処理
def getCandleDataFromOanda(instrument, api, date_from, date_to, granularity):
    params = {
        "from": date_from.isoformat(),
        "to": date_to.isoformat(),
        "granularity": granularity,
    }
    r = instruments.InstrumentsCandles(instrument=instrument, params=params)
    return api.request(r)

#取得したcandleデータを配列に変換する処理
def oandaJsonToPythonList(JSONRes):
    data = []
    for res in JSONRes['candles']:
        data.append( [
            datetime.datetime.fromisoformat(res['time'][:19]),
            res['volume'],
            res['mid']['o'],
            res['mid']['h'],
            res['mid']['l'],
            res['mid']['c'],
            ])
    return data

all_data = []
# year, monthでループ
for year, month in year_months:
    date_from = datetime.datetime(year, month, 1)
    date_to = date_from + relativedelta(months=+1, day=1)

    ret = getCandleDataFromOanda("USD_JPY", api, date_from, date_to, "M10")
    month_data = oandaJsonToPythonList(ret)

    all_data.extend(month_data)

#取得したデータをPandas DataFrameへ変換
df = pd.DataFrame(all_data)
df.columns = ['Datetime', 'Volume', 'Open', 'High', 'Low', 'Close']
df = df.set_index('Datetime')

# CSVファイルへの出力
df.to_csv('./USD_JPY_201901-201912_M10.csv')

# CSVファイルの読み取り
df = pd.read_csv('./USD_JPY_201901-201912_M10.csv', index_col='Datetime')